<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Post;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         
        // Create users for testing
        $userId = User::factory(1)->create()->first()->id; 
        
        // Test the post model can insert a message
        Post::create([
            'author_id' => $userId,
            'title' => 'title',
            'message' => "Test 1 Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book",
        ]);
    }
}
