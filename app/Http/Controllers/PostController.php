<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\User;
use App\Http\Resources\PostResources;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    private $maxTextLength;

    public function __construct()
    {
        $this->maxTextLength = 255;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get all the messages
        return PostResources::collection(Post::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validated = Validator::make($request->all(), [
                'author_id' => 'required',
                'message' => 'required|max:255',
            ]);

            if ($validated->fails()) {
                return response()->json(['error' => $validated->errors()]);
            } else {
                $message = Post::create($request->all());
                return response()->json($message, 201);
            }
        }
        //catch exception
        catch(\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 406);
        } 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Displaying massage based on an author
        try {
            $checkUser = User::find($id);

            if (is_null($checkUser)) {
                return response()->json(['error' => ['message' => 'Invalid User']], 401);
            } else {
                $posts = Post::where('author_id', $id)
                    ->get()
                    ->map(function($posts) {
                        return PostResources::make($posts);
                    }); 
                return response()->json($posts, 201);
            }
        }
        //catch exception
        catch(\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 406);
        }
    }

    /**
     * Show the list of messages
     *
     * @return \Illuminate\Http\Response
     */
    public function display()
    {
        $posts = Post::all()
            ->map(function ($post){
                return (object) [
                    'id'            => $post->id,
                    'author'        => User::find($post->author_id)->name,
                    'title'         => $post->title,
                    'message'       => $post->message,
                    'created_at'    => date("F j, Y, g:i a", strtotime($post->created_at))
                ];
            });
       
        return view('welcome', compact('posts'));
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy($id)
    {
        // Destroy post incase we need to delete
        $post = Post::find($id);
        $post->delete();
    }
}
