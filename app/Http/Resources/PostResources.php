<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\User;

class PostResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'author'        => User::find($this->author_id)->name,
            'title'         => $this->title,
            'message'       => $this->message,
            'created_at'    => date("F j, Y, g:i a", strtotime($this->created_at))
        ];
    }
}
