<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Post;
use App\Models\User;

class PostTest extends TestCase
{
    use RefreshDatabase;
 
    /**
     * Test the post can be retreived 
     */
    public function test_the_post_can_be_retrieved_correctly()
    {
        // create users
        $user  = User::factory(1)->create()->first();
        $title = 'Title';
        $post  = 'Testing the post can be retrieved';

        // Create a post
        $data = [
            'author_id' => $user->id,
            'title'     => $title,
            'message'   => $post,
        ];

        // Checking we we can retrieve the post
        $this->get(('/api/message'))
             ->assertSuccessful();
    }

    /**
     * Test the storing of post
     */
    public function test_the_post_can_store_information()
    {
        // create users
        $user  = User::factory(1)->create()->first();
        $title = 'Title';
        $post  = 'Testing the post can be stored';

        // Create a post
        $data = [
            'author_id' => $user->id,
            'title'     => $title,
            'message'   => $post,
        ];

        // Checking we we can store the post
        $this->post(('/api/message/store'), $data)
            ->assertStatus(201);
    }

    /**
     * Test the storing of post if the length is validated
     */
    public function test_the_post_message_is_less_than_the_length()
    {
        // create users
        $user       = User::factory(1)->create()->first();
        $title      = "Post Title";
        $longPost   = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book red is the new grreeb but make it possble";

        // Create a post
        $data = [
            'author_id' => $user->id,
            'title'     => $title,
            'message'   => $longPost,
        ];

        $expectedData = ['error' => ['message' => ["The message may not be greater than 255 characters."]]];
        $response = $this->post(('/api/message/store'), $data);
        $response->assertJsonFragment($expectedData, $strict = false);
    }

    /**
     * Test the showing of the post with no author
     */
    public function test_show_message_with_no_author()
    {
        // Sending request with no author id
        $response = $this->post(('/api/message/show/'));
        $response->assertStatus(404);
    }
    
    /**
     * Test the showing of the post with an invalid author
     */
    public function test_show_message_with_invalid_author()
    {
        // Sending request with a non existance author
        $id = 1;
        $expectedData = ['error' => ['message' => "Invalid User"]];
        $response = $this->post(('/api/message/show/' . $id));
        $response->assertJsonFragment($expectedData, $strict = false);
    }

    /**
     * Test the showing of the post with a valid author
     */
    public function test_show_message_with_valid_author()
    {
        // create users
        $user  = User::factory(1)->create()->first();
        $title = 'Title';
        $post  = 'Testing the post with a valid author';

        // Create a post
        $data = [
           'author_id' => $user->id,
           'title'     => $title,
           'message'   => $post,
        ];
    
        $response = $this->post(('/api/message/show/' . $user->id), $data);
        $response->assertStatus(201);
    }

    /**
     * Test the showing of the post with a valid author
     */
    public function test_post_can_be_deleted()
    {
        // Add a new post
        $user  = User::factory(1)->create()->first();
        $title = 'Title';
        $post  = 'Testing the post with a valid author';
        
        $post  = Post::create([
            'author_id' => $user->id,
            'title'     => $title,
            'message'   => $post,
        ])->first();

        // Delete the post by id
        $response = $this->post(('/api/message/destroy/' . $post->id));
        $response->assertStatus(200);
    }

}

